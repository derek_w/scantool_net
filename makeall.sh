#!/bin/bash

PrintHelp()
{
cat <<EOM
   -------------------------------------------------------------------------
      Usage: $(basename $0)  [-r] [-d] [-v BUILD_VERSION]

         -h       Print [h]elp and exit.
         -r       Build for [r]elease
         -d       Build for [d]ebug
         -v       Specify the build [v]ersion
   -------------------------------------------------------------------------
EOM

   exit 0
}

declare RELEASE=""
declare BUILD_VERSION=121

#Necessary so the proper CFLAGS are used
export MINGDIR=NULL

while getopts hrdv: opt
do
   case "$opt" in
     h) PrintHelp;;
     r) RELEASE="release";;
     d) export DEBUGMODE=1;;
     v) BUILD_VERSION=$OPTARG;;
   esac
done


if [ ! -f allegro-mingw-4.2.3.zip ]; then
   wget http://static.allegro.cc/file/library/allegro-4.2.3/allegro-mingw-4.2.3.zip
fi

if [ ! -d allegro/ ]; then
   mkdir allegro
   unzip allegro-mingw-4.2.3.zip -d allegro/
fi


if [ ! -f dx70_mgw.zip ]; then
   wget http://alleg.sourceforge.net/files/dx70_mgw.zip
fi

if [ ! -d dx70_allegro/ ]; then
   mkdir dx70_allegro
   unzip dx70_mgw.zip -d dx70_allegro/
fi

if [ ! -f /usr/bin/i586-mingw32msvc-gcc ]; then
   echo "Missing MinGW gcc compiler,"
   echo " please install gcc-mingw32"
   exit 0
fi

make clean
make veryclean

make $RELEASE

if [ -f ScanTool.exe ]; then
   if [ -f ScanTool_${BUILD_VERSION}.tar ]; then
      rm ScanTool_${BUILD_VERSION}.tar
   fi

   tar -cvf ScanTool_${BUILD_VERSION}.tar ScanTool.exe scantool.dat -C allegro/allegro-mingw-4.2.3/bin .
   echo "Scantool Package Created Successfully"
else
   echo "Package Not Created Successfully"
fi
